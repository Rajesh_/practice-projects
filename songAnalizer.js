/**
 * Get the URL for Rafi's song list that matches with your first letter of your name
 * Main songs list url: https://en.wikipedia.org/wiki/List_of_songs_recorded_by_Mohammed_Rafi
 * Make a request to get data from the given URL and find the link that is relavent for 
 * list of songs
 * 
 * @params startLetter - This is the letter to get the link for
 * @return URL for list of songs matches with first letter of your name
 */
const rp = require('request-promise');
const $ = require('cheerio');
const fetch = require('node-fetch');

//main Url
let url = "https://en.wikipedia.org/wiki/List_of_songs_recorded_by_Mohammed_Rafi";
let songsLinks = [];
let songObject = [];
let count = 0;
listCount=1;
let yearObject = [];


async function getRafiMusicLink(string) {

    string = string.toUpperCase();
    let html = await rp(url); //request-promis
    $('.mw-parser-output>ul>li>a[href]', html).each((index, elem) => {
        songsLinks.push("https://en.wikipedia.org" + $(elem).attr('href'));
    });  //get array of all links

    let songsLink = songsLinks[0].replace(/(\([A-Z]\))/, `(${string})`); //when linlk exists directly
    console.log(`\ndirect link is ` + songsLink);


    //when link doesnt exits 
    string = String.fromCharCode(string.charCodeAt(0) + 1);
    stringEnd = String.fromCharCode(string.charCodeAt(0) + 1);
    songsLink = songsLinks[0].replace(/(\([A-Z]\))/, `(${string}%E2%80%93${stringEnd})`);
    console.log('found next link:' + songsLink + "\n\n");
    return songsLink;

}


async function getListOfSongs() {
    let url = "https://en.wikipedia.org/wiki/List_of_songs_recorded_by_Mohammed_Rafi_(B%E2%80%93C)"
    let html = await rp(url);
    while(true) 
    {  listCount++;

        try{

        let str = $(`.columns > ul:nth-child(3) > li:nth-child(${listCount})`, html).text();
        var regex = /([a-zA-z\s,]+)\(/g;
        var songName = str.match(regex);
        songNameStr = songName[0];
        songNameStr = songNameStr.slice(0, -1);
        // console.log("song name:" + songNameStr+"\n\n");


        var regex = /\) -[a-zA-z\s,]+/g;
        var movie = str.match(regex);
        movieNameString = movie[0];
        var movieNameStringNew = movieNameString.substr(3);
        // console.log("movie name:" + movieNameStringNew+"\n\n");
        var regex = /\d{4}/g;
        var year = str.match(regex);
        yearString = year[0];
        // console.log("year :" + yearString+"\n\n");

        songObject.push({ name: songNameStr, movie: movieNameStringNew, year: yearString });
        console.log(songObject);
        }
        catch(err){ return console.log("Searching Over");}

    }
    // console( songObject);
}
async function listOfSongsInYear(songObject, year) {
    songObject.forEach(i => {
        if (i.year == year) {
            console.log(i.name);
            count = count + 1;
        }

    });
    console.log(`\nsong sung in the year ` + count);
    return (count);
}
async function mostNumberOfSongs(songObject) {
    let summarySongsData = {};
    songObject.forEach(element => {
        if (!summarySongsData[element.year]) {
            summarySongsData[element.year] = 1;
        } else {
            summarySongsData[element.year] += 1;
        }
    });
    //to print tabl of array
    console.table(summarySongsData);
    //to get maximun year
    var max = Math.max.apply(null, Object.keys(summarySongsData).map(function (x) { return summarySongsData[x] }));
    let maxYear = Object.keys(summarySongsData).filter(function (x) { return summarySongsData[x] == max; })[0];
    console.log(`Heighest number of songs was in year ` + maxYear + ` : ` + summarySongsData[maxYear]);


};

async function main() {

    let songsLink = await getRafiMusicLink('A');
    let songObject = await getListOfSongs();
    let year = '1966';
    await listOfSongsInYear(songObject, year);
    mostNumberOfSongs(songObject);
}

main();



// let stringStartCharCode = stringEndCharCode = 0;

//     firstLetter = firstLetter.toUpperCase();
//     let html = await rp(url); //request-promis
//     $('.mw-parser-output>ul>li>a[href]', html).each((index, elem) => {
//         songsLinks.push("https://en.wikipedia.org" + $(elem).attr('href'));
//     });  //get array of all links
//     FirstLetterCharCode = firstLetter.charCodeAt(0);

//     if (FirstLetterCharCode >=66 && FirstLetterCharCode <= 67) { stringStartCharCode = 66; stringEndCharCode = 67; }
//     else if (FirstLetterCharCode >=68 && FirstLetterCharCode <= 70) { stringStartCharCode = 68; stringEndCharCode = 70; }
//     else if (FirstLetterCharCode >=72 && FirstLetterCharCode <= 73) { stringStartCharCode = 72; stringEndCharCode = 73; }
//     else if (FirstLetterCharCode >=80 && FirstLetterCharCode <= 83) { stringStartCharCode = 80; stringEndCharCode = 83; }
//     else if (FirstLetterCharCode >= 85 && FirstLetterCharCode <= 90) { stringStartCharCode = 85; stringEndCharCode = 90;}
//     else { return songsLinks[0].replace(/(\([A-Z]\))/, `(${firstLetter})`); }
//     return songsLinks[0].replace(/(\([A-Z]\))/, `(${firstLetter.fromCharCode(stringStartCharCode)}%E2%80%93${firstLetter.fromCharCode(stringEndCharCode)})`);


